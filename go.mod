module test

go 1.13

require (
	github.com/gobuffalo/packr v1.30.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-runewidth v0.0.5 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.1
	github.com/pkg/errors v0.8.0
	github.com/rubenv/sql-migrate v0.0.0-20191022111038-5cdff0d8cc42
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	github.com/ziutek/mymysql v1.5.4 // indirect
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/errgo.v2 v2.1.0
	gopkg.in/gorp.v1 v1.7.2 // indirect
)
